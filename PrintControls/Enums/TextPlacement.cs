namespace PrintControl
{
    /// <summary>
    /// Enumeration for text placement
    /// </summary>
    public enum TextPlacement
    {
        /// <summary>
        /// No placement
        /// </summary>
        None,
        /// <summary>
        /// Header
        /// </summary>
        Header,
        /// <summary>
        /// Footer
        /// </summary>
        Footer
    }

}