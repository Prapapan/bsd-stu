

using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace PrintControl
{

    /// <summary>
    /// PrintDocumentComponent
    /// </summary>
    public partial class PrintDocumentComponent : PrintDocument
    {
        private Control _controlToPrint;
        private PrintDocumentSettings _printDocumentSettings;
        private Size _defaultSize = new Size(600, 800);
        private int _width;
        private int _height;
        private int _printedheight;
        private List<TreeNode> _nodes;

        private int _pageCounter;
        private int _pageTotal;

        # region Constructors

        /// <summary>
        /// Intialize the component with the selected control
        /// </summary>
        /// <param name="controlToPrint">The control to print.</param>
        /// <param name="printDocumentSettings">The print document settings.</param>
        public PrintDocumentComponent(Control controlToPrint, 
                                      PrintDocumentSettings printDocumentSettings)
        {
            InitializeComponent();
            _printDocumentSettings = printDocumentSettings;
            InitializeControl(controlToPrint);
        }

        private void InitializeControl(Control controlToPrint)
        {
            _controlToPrint = controlToPrint;

            Size size = CalculateSize();
            
            _width = size.Width;
            _height = size.Height;
            _printedheight = 0;
        }

        # endregion Constructors

        # region Properties

        /// <summary>
        /// The _width of the control to print. You can change this value if the automatically 
        /// calculated _width does not fit all the elements of the control
        /// </summary>
        public int PrintWidth
        {
            get { return _width; }
            set { _width = value; }
        }

        /// <summary>
        /// The _height of the control to print. You can change this value if the automatically
        /// calculated _height does not fit all the elements of the control
        /// </summary>
        public int PrintHeight
        {
            get { return _height; }
            set { _height = value; }
        }

        /// <summary>
        /// Gets or sets the page total.
        /// </summary>
        /// <value>The page total.</value>
        public int PageTotal
        {
            get { return _pageTotal; }
            set { _pageTotal = value; }
        }

        /// <summary>
        /// The area to be reprinted between pages if there are more than one
        /// pages to be printed, to prevent data loss
        /// </summary>
        private int OverlapArea
        {
            get { return _printDocumentSettings.OverlapArea; }
        }

        /// <summary>
        /// Set true to _stretch the control to fill a single printed page
        /// </summary>
        private bool StretchControl
        {
            get { return _printDocumentSettings.StretchToFit; }
        }

        # endregion Properties

        # region Public methods

        # region Event Handlers

        /// <summary>
        /// Called when [begin print].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="T:System.Drawing.Printing.PrintEventArgs"/> instance containing the event data.</param>
        private void OnBeginPrint(object sender, PrintEventArgs e)
        {
            _pageCounter = 1;
            _pageTotal = 1;
        }

        /// <summary>
        /// Handles the PrintPage event of the PrintDocumentComponent control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.Drawing.Printing.PrintPageEventArgs"/> instance containing the event data.</param>
        private void OnPrintPage(object sender, PrintPageEventArgs e)
        {

            //Get current status of the control
            Size newSize = new Size(_width, _height);

            DockStyle oldDock = _controlToPrint.Dock;           
            Size oldSize = new Size(_controlToPrint.Width, _controlToPrint.Height);
            Control parent = _controlToPrint;
            List<Control> parents = new List<Control>();
            List<Size> oldSizes = new List<Size>();
            
            //Enumerate the parents
            while (parent.Parent != null)
            {
                parents.Add(parent.Parent);
                oldSizes.Add(parent.Parent.Size);
                parent = parent.Parent;
            }

            //Change the size of the control to fully display it and get rid of scrollbars
            _controlToPrint.Dock = DockStyle.None;
            _controlToPrint.Size = newSize;

            //Make sure that the size changes otherwise resize the parents
            while (_controlToPrint.Size == oldSize)
            {
                foreach (Control c in parents)
                {
                    c.Size = new Size(c.Width , c.Height );
                }

                newSize = new Size(newSize.Width + 1, newSize.Height + 1);
                _controlToPrint.Size = newSize;
            }

            //Print dimensions will be according to page size and margins
            int printwidth = _width;
            int printheight = _height;

            //Change width to fit paper and change height to maintain ratio
            if (printwidth > e.MarginBounds.Width)
            {
                printheight = (int)(((float)e.MarginBounds.Width / (float)printwidth) * printheight);
                printwidth = e.MarginBounds.Width;
            }

            //Determine if more pages are required
            if (printheight - _printedheight > e.MarginBounds.Height &&
                !StretchControl)
                e.HasMorePages = true;
            else
                e.HasMorePages = false;

            //Graphics variables
            GraphicsUnit graphicsUnit = GraphicsUnit.Point;
            Bitmap bitmap = new Bitmap(_width, _height);

            //Draw the bitmap
            _controlToPrint.DrawToBitmap(bitmap, new Rectangle(0, 0, _width, _height));

            //float fontSize;
            //int distVertical;
            //int distHorizontal;

            ////Header
            //fontSize = _printDocumentSettings.HeaderFont.SizeInPoints * (float)0.625; // 0.625 is a rule of thumb percentage averaged for fonts
            //string header = _printDocumentSettings.HeaderText;
            //e.Graphics.DrawString(_printDocumentSettings.HeaderText,
            //                      _printDocumentSettings.HeaderFont,
            //                      new SolidBrush(_printDocumentSettings.HeaderColor),
            //                      //width : centered alignment
            //                      ((e.PageBounds.Width / 2) - ((header.Length * fontSize) / 2)),
            //                      //distance from top of page
            //                      30);

            ////Footer
            //fontSize = _printDocumentSettings.FooterFont.SizeInPoints * (float)0.625;
            //string footer = _printDocumentSettings.FooterText;
            //e.Graphics.DrawString(footer,
            //                      _printDocumentSettings.FooterFont,
            //                      new SolidBrush(_printDocumentSettings.FooterColor),
            //                      //width : centered alignment
            //                      ((e.PageBounds.Width / 2) - ((footer.Length * fontSize) / 2)),
            //                      //height minus a fixed distance from bottom of page
            //                      e.PageBounds.Height - 50);

            ////Page number
            //distVertical = 0;
            //distHorizontal = 0;
            //string pagenr = "Page " + _pageCounter;
            //switch (_printDocumentSettings.PageNumberPlacement)
            //{
            //    case TextPlacement.Header:
            //        distVertical = 30;
            //        break;
            //    case TextPlacement.Footer:
            //        distVertical = e.PageBounds.Height - 50;
            //        break;
            //    case TextPlacement.None:
            //        distVertical = 0;
            //        break;
            //}           
            //switch (_printDocumentSettings.PageNumberAlignment)
            //{
            //    case TextAlignment.Left:
            //        distHorizontal = 20;
            //        break;
            //    case TextAlignment.Right:
            //        distHorizontal = e.PageBounds.Width - 20 - (int)(pagenr.Length * _printDocumentSettings.PageNumberFont.Size);
            //        break;
            //}
            //if (distVertical != 0)
            //{
            //    e.Graphics.DrawString(pagenr,
            //                          _printDocumentSettings.PageNumberFont,
            //                          new SolidBrush(_printDocumentSettings.PageNumberColor),
            //                          distHorizontal,
            //                          distVertical);
            //}

            ////Print date

            //string date = _printDocumentSettings.FormatDateTime(System.DateTime.Now);

            //distVertical = 0;
            //distHorizontal = 0;
            //switch (_printDocumentSettings.DatePlacement)
            //{
            //    case TextPlacement.Header:
            //        distVertical = 30;
            //        break;
            //    case TextPlacement.Footer:
            //        distVertical = e.PageBounds.Height - 50;
            //        break;
            //    case TextPlacement.None:
            //        distVertical = 0;
            //        break;
            //}
            //switch (_printDocumentSettings.DateAlignment)
            //{
            //    case TextAlignment.Left:
            //        distHorizontal = 20;
            //        break;
            //    case TextAlignment.Right:
            //        distHorizontal = e.PageBounds.Width - 20 - (int)(date.Length * _printDocumentSettings.DateFont.Size); ;
            //        break;
            //}
            //if (distVertical != 0)
            //{
            //    e.Graphics.DrawString(date,
            //                          _printDocumentSettings.DateFont,
            //                          new SolidBrush(_printDocumentSettings.DateColor),
            //                          distHorizontal,
            //                          distVertical);
            //}

            //To stretch or not to stretch
            if (StretchControl)
            {
                e.Graphics.DrawImage(bitmap, e.MarginBounds, bitmap.GetBounds(ref graphicsUnit), graphicsUnit);
                e.HasMorePages = false;
            }
            else
            {
                //If not stretched then make sure to print the area of the current page only
                float ScaleF = (float) _height/(float) printheight;
                printheight -= _printedheight;
                if (printheight > e.MarginBounds.Height)
                    printheight = e.MarginBounds.Height;
                Rectangle rect =
                    new Rectangle(0, (int) (_printedheight*ScaleF), bitmap.Width, (int) (printheight*ScaleF));
                Bitmap bitmapClone = bitmap.Clone(rect, PixelFormat.DontCare);
                e.Graphics.DrawImage(bitmapClone,
                                     new Rectangle((e.PageBounds.Width/2) - (printwidth/2), e.MarginBounds.Top,
                                                   printwidth, printheight));
            }

            if (e.HasMorePages)
            {
                //Change the printed height, subtract the overlap area
                _printedheight += e.MarginBounds.Height - OverlapArea;
            }
            else
            {
                _printedheight = 0;
            }

            //Restore the control's state
            for (int i = 0; i < parents.Count; i++)
                parents[i].Size = oldSizes[i];
            _controlToPrint.Size = new Size(oldSize.Width, oldSize.Height);
            _controlToPrint.Dock = oldDock;

            if (e.HasMorePages)
            {
                _pageCounter++;
                _pageTotal++;
            }

        }

        /// <summary>
        /// Called when [end print].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="T:System.Drawing.Printing.PrintEventArgs"/> instance containing the event data.</param>
        private void OnEndPrint(object sender, PrintEventArgs e)
        {
            //no action
        }

        # endregion Event Handlers

        /// <summary>
        /// Draw the control fully to a bitmap and return it
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public Bitmap GetBitmap()
        {
            //Get old states
            Size newSize = new Size(_width, _height);
            DockStyle oldDock = _controlToPrint.Dock;
            Size oldSize = new Size(_controlToPrint.Width, _controlToPrint.Height);
            
            Control parent = _controlToPrint;
            List<Control> parents = new List<Control>();
            List<Size> oldSizes = new List<Size>();
            while (parent.Parent != null)
            {
                parents.Add(parent.Parent);
                oldSizes.Add(parent.Parent.Size);
                parent = parent.Parent;
            }

            _controlToPrint.Dock = DockStyle.None;
            _controlToPrint.Size = newSize;
            while (_controlToPrint.Size == oldSize)
            {
                foreach (Control c in parents)
                {
                    c.Size = new Size(c.Width + 100, c.Height + 100);
                }
                newSize = new Size(newSize.Width + 1, newSize.Height + 1);
                _controlToPrint.Size = newSize;
            }

            Bitmap bitmap = new Bitmap(_width, _height);
            
            //Draw
            _controlToPrint.DrawToBitmap(bitmap, new Rectangle(0, 0, _width, _height));
            
            //Restore states
            for (int i = 0; i < parents.Count; i++)
                parents[i].Size = oldSizes[i];
            _controlToPrint.Size = new Size(oldSize.Width, oldSize.Height);
            _controlToPrint.Dock = oldDock;
            
            return bitmap;
        }

        /// <summary>
        /// Return the best size that fits the control
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public Size CalculateSize()
        {
            //Identify the control's type
            if (_controlToPrint.GetType().AssemblyQualifiedName.IndexOf("TreeView") >= 0)
            {
                TreeView control = _controlToPrint as TreeView;
                if (control != null)
                {
                    if (control.Nodes.Count == 0)
                    {
                        //if no elements return 1x1 to avoid exceptions
                        return _defaultSize;
                    }
                    else
                    {
                        int tempWidth = 0;
                        _nodes = new List<TreeNode>();
                        foreach (TreeNode treeNode in control.Nodes)
                            EnumNodes(treeNode);
                        foreach (TreeNode node in _nodes)
                        {
                            if (tempWidth < node.Bounds.Right)
                                tempWidth = node.Bounds.Right;
                        }
                        int tempHeight = _nodes.Count * control.ItemHeight;
                        return new Size(tempWidth + 30, tempHeight + 30);
                    }
                }
            }
            else if (_controlToPrint.GetType().AssemblyQualifiedName.IndexOf("ListView") >= 0)
            {
                ListView control = _controlToPrint as ListView;
                if (control != null)
                {
                    if (control.Items.Count > 0)
                    {
                        //This will work if the listview is in details mode
                        control.Items[0].EnsureVisible();
                        int tempWidth = control.Items[0].Bounds.Width;
                        int tempHeight = control.Items[control.Items.Count - 1].Bounds.Bottom;
                        //This is for other modes
                        foreach (ListViewItem i in control.Items)
                        {
                            if (tempWidth < i.Bounds.Right)
                                tempWidth = i.Bounds.Right;
                            if (tempHeight < i.Bounds.Bottom)
                                tempHeight = i.Bounds.Bottom;
                        }
                        return new Size(tempWidth + 30, tempHeight + 30);
                    }
                    else
                        return _defaultSize;
                }
            }
            else if (_controlToPrint.GetType().AssemblyQualifiedName.IndexOf("PropertyGrid") >= 0)
            {
                PropertyGrid control = _controlToPrint as PropertyGrid;

                if (control !=  null)
                {
                    control.ExpandAllGridItems();

                    int count = control.SelectedObject.GetType().GetProperties().Length;

                    return new Size(control.Right, (count * 15));
                }
                else
                    return _defaultSize;
            }
            else if (_controlToPrint.Controls.Count > 0)
            {
                //Just in case the form didn't calculate its PreferredSize properly
                int tempWidth = 1;
                int tempHeight = 1;
                foreach (Control c in _controlToPrint.Controls)
                {
                    if (c.Bounds.Right > tempWidth)
                        tempWidth = c.Bounds.Right;
                    if (c.Bounds.Bottom > tempHeight)
                        tempHeight = c.Bounds.Bottom;
                }
                return new Size(tempWidth, tempHeight);
            }
            else
            {
                try
                {
                    return new Size(_controlToPrint.PreferredSize.Width, 
                                    _controlToPrint.PreferredSize.Height);
                }
                catch
                {
                    //for controls that do not support a PreferredSize properly
                    return _defaultSize;
                }
            }
            return _defaultSize;
        }

        # endregion Public methods

        # region Helpers

        private void EnumNodes(TreeNode treeNode)
        {
            if (!_nodes.Contains(treeNode) && (treeNode.Parent == null ||
                (treeNode.Parent != null && treeNode.Parent.IsExpanded)))
            {
                _nodes.Add(treeNode);
            }

            //Enum all subnodes of the current node
            foreach (TreeNode node in treeNode.Nodes)
            {
                EnumNodes(node);
            }
        }

        # endregion Helpers
    }
}
