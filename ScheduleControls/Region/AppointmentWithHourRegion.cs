﻿/*
    Copyright 2011 Andrew Sydney
 
    This file is part of ScheduleControls.

    ScheduleControls is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ScheduleControls is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with ScheduleControls.  If not, see <http://www.gnu.org/licenses/>.
*/
using System.Collections.Generic;
using ScheduleControls.Data;

namespace ScheduleControls.Region
{
    internal class AppointmentWithHourRegion : AppointmentRegion
    {
        public AppointmentWithHourRegion(Appointment appointment) : base(appointment)
        {
        }
        public bool HasOverlaps { get; set; }
        public int StartingSlot { get; set; }
        public int TotalSlots { get; set; }
        public List<AppointmentWithHourRegion> OverlappingAppointments = new List<AppointmentWithHourRegion>();
    }
}