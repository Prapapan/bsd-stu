﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar._20Desing;
using WrapCar._30Appointment;

namespace ImageGetArea
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void designCarToolStripMenuItem_Click(object sender, EventArgs e)
        {
           //DesignForm dForm = new DesignForm();
           //dForm.MdiParent = this;
           //pnlMainBody.Controls.Add(dForm);
           //dForm.Dock = DockStyle.Fill;
           //dForm.Show();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.tabMain.Width = this.Width;
            this.tabMain.Height = this.Height;
            displayDesignForm();
        }

        private void tabDesign_SelectedIndexChanged(object sender, EventArgs e)
        {
            int tapIdx = this.tabMain.SelectedIndex;

            if (tapIdx == 0)
            {
                displayDesignForm();
            }
            else if (tapIdx == 1)
            {
                displayAppointmentForm();
            }
        }

        DesignForm dForm;
        AppointmentForm appForm;

        //Design Form
        private void displayDesignForm()
        {
            if (dForm == null)
            {
                dForm = new DesignForm();
                dForm.MdiParent = this;

                dForm.Width = this.tabMain.Width;
                dForm.Height = this.tabMain.Height;
                tabPageDesign.Controls.Add(dForm);
                dForm.Dock = DockStyle.Fill;
            }
            dForm.Show();
        }

        //Appointment Form
        private void displayAppointmentForm()
        {
            if (appForm == null)
            {
                appForm = new AppointmentForm();
                appForm.MdiParent = this;

                appForm.Width = this.tabMain.Width;
                appForm.Height = this.tabMain.Height;
                tabPageAppointment.Controls.Add(appForm);
                appForm.Dock = DockStyle.Fill;
            }
            appForm.Show();
        }
        

    }
}
