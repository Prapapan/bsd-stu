﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using PrintControl;

namespace WrapCar._20Desing
{
    public partial class ArrangeStickerForm : Form
    {
        int stkRollWidth;
        int stkRollHeight;
        private Dictionary<string, StickerDetection> mapDicutObj;

        public ArrangeStickerForm()
        {
            InitializeComponent();
        }

        public ArrangeStickerForm(Dictionary<string, StickerDetection> mapDicutObj)
        {
         
            InitializeComponent();
            this.mapDicutObj = mapDicutObj;
            this.monthCalendar1.SelectionRange = new SelectionRange(
                                    ImageUtilities.StartOfWeek(DateTime.Now, DayOfWeek.Sunday)
                                    , ImageUtilities.EndOfWeek(DateTime.Now, DayOfWeek.Saturday));

            this.LoadOrderTreeView(this.monthCalendar1.SelectionRange.Start, this.monthCalendar1.SelectionRange.End);
        }
  
        private void ArrangeStickerForm_Load(object sender, EventArgs e)
        {
          this.WindowState = FormWindowState.Maximized;
        }

        private void btnLoadSticker_Click(object sender, EventArgs e)
        {
            this.arrangeStickerMain();
        }

        public void arrangeStickerMain()
        {
            //Step 0: Load sticker image from order.
            Bitmap[] bmps = this.getStickerFilesFromOrder();

            //Step 1: Arrange Sticker By Size
            List<List<Piece>> stkTable = this.ArrangeStickerController(bmps);
            bool isEmty = (stkTable == null || stkTable.Count == 0);
            if (!isEmty)
            {
                //Step 2: Return for Display sticker items on paper roll.
                this.displayArrangeStickerPiece(stkTable);
            }
            else
            {
                DialogResult result = MessageBox.Show("ไม่พบรายการสั่งทำสติ๊กเกอร์วันนี้ กรุณาตรวจสอบใหม่.");
                this.Close();
            }
        }

        private List<List<Piece>> ArrangeStickerController(Bitmap[] bmps)
        {
            List<List<Piece>> stkTable = null;

            this.stkRollWidth = this.pnlOutput.Width;
            this.stkRollHeight = this.pnlOutput.Height;


            List<Bitmap> bmpsRaw = bmps.ToList();

            //Step 1.1: Ordering sticker items before arrange on paper roll.
            bmpsRaw = this.sortingBySizeStickerFiles(bmpsRaw);

            //Step 1.2: Arrange sticker items  on paper roll.
            stkTable = this.arrangeStickerPiecePuzzle(bmpsRaw);
  
            return stkTable;
        }

        private Bitmap[] getStickerFilesFromOrder()
        {
            string path = Constants.BASE_PATH + Constants.DRAFT_DIR + "/" + Constants.orderID;
            Bitmap[] bmps = null;

            try
            {
                string[] filePaths = Directory.GetFiles(path, Constants.STK_PRINT_CUTOFF_PREFIX + "*.png", SearchOption.AllDirectories);
                bmps = new Bitmap[filePaths.Length];

                int i = 0;
                foreach (string imgPath in filePaths)
                {
                    bmps[i] = (Bitmap)Image.FromFile(imgPath);
                    i++;
                }

            } catch(Exception ex){
                Console.WriteLine( ex.StackTrace);
            }

            return bmps;
        }

        private List<Bitmap> sortingBySizeStickerFiles(List<Bitmap> bmpsRaw)
        {
            //Order by width DESC
            bmpsRaw = bmpsRaw.OrderByDescending(t => t.Width).ToList();
            return bmpsRaw;
        }

        private List<List<Piece>> arrangeStickerPiecePuzzle(List<Bitmap> bmpsRaw)
        {
            List<List<Piece>> hashRowtable = new List<List<Piece>>();
           
            //Iniitial sticker set in the first row
            List<Piece> stkBmpCols = new List<Piece>();
            hashRowtable.Add(stkBmpCols);

            int accWidth = 0;
            int accHeight = 0;
            int rowAt = 0;
            int pieceID = 0;
            int stX = 0;
            int stY = 0;

            //Recursive Arrange
            hashRowtable = this.arrangeWithRecursive(bmpsRaw
                                                   , hashRowtable
                                                   , stkBmpCols
                                                   , accWidth
                                                   , accHeight
                                                   , rowAt
                                                   , pieceID
                                                   , stX
                                                   , stY
                                                   );

            return hashRowtable;
        }

        private List<List<Piece>> arrangeWithRecursive(   List<Bitmap> bmpsRaw
                                                        , List<List<Piece>> hashRowtable
                                                        , List<Piece> stkBmpCols
                                                        , int accWidth
                                                        , int accHeight
                                                        , int rowAt
                                                        , int pieceID
                                                        , int stX
                                                        , int stY) {
        
            foreach (Bitmap bmp in bmpsRaw)
            {
                accWidth += bmp.Width;
                accHeight += bmp.Height;

                bool overWidth = (accWidth >= this.stkRollWidth);
              
                //Add sticker item in the row
                if (!overWidth)
                {
                    Piece piece = new Piece
                    {
                        ID = pieceID,
                        Width = bmp.Width,
                        Height = bmp.Height,
                        startX = stX,
                        startY = stY,
                        Picture = bmp
                    };

                    stkBmpCols.Add(piece);
                    bmpsRaw.Remove(bmp);
                    pieceID++;

                    hashRowtable[rowAt] = stkBmpCols;
                    stX = accWidth;
                }

                //Create sticker item in the new row
                else
                {
                    rowAt++;

                    accWidth = bmp.Width;
                    accHeight = 0;
                    stX = 0;
                    stY += stkBmpCols.Max(r => r.Height); //Max height at row-1

                    stkBmpCols = new List<Piece>();
                    Piece piece = new Piece
                    {
                        ID = pieceID,
                        Width = bmp.Width,
                        Height = bmp.Height,
                        startX = stX,
                        startY = stY,
                        Picture = bmp
                    };
                   
                    stkBmpCols.Add(piece);
                    bmpsRaw.Remove(bmp);
                    
                    pieceID++;

                    hashRowtable.Add(stkBmpCols);
                    stX = accWidth;
                }
                
                this.arrangeWithRecursive(bmpsRaw
                                            , hashRowtable
                                            , stkBmpCols
                                            , accWidth
                                            , accHeight
                                            , rowAt
                                            , pieceID
                                            , stX
                                            , stY
                                            );
                if (bmpsRaw.Count == 0) break;
            }

            return hashRowtable;
       }

        private void displayArrangeStickerPiece(List<List<Piece>> hashtable)
        {
            Bitmap bb = new Bitmap(pnlOutput.Width, pnlOutput.Height);
            pnlOutput.Image = bb;
            Graphics e = Graphics.FromImage(bb);

            float[] dashValues = { 2, 2, 2, 2 };
            Pen blackPen = new Pen(Color.Gray, 1);
            blackPen.DashPattern = dashValues;

            #region print data
            int i = 0;
            foreach (List<Piece> bmpList in hashtable)
            {            
                foreach (Piece piece in bmpList)
                {
                    //Draw Image
                    e.DrawImage(piece.Picture, piece.startX, piece.startY);

                    //Draw Image Border
                    //e.DrawRectangle(blackPen, piece.startX, piece.startY, piece.Width, piece.Height);
                 }
                i++;
            }
            #endregion

           // pnlOutput.Invalidate();
           // textBox1.Text = str;
        }

        private void pnlOutput_MouseDown(object sender, MouseEventArgs e)
        {
           
        }

        private void pnlOutput_MouseMove(object sender, MouseEventArgs e)
        {
           
        }

        private void pnlOutput_MouseUp(object sender, MouseEventArgs e)
        {
           
        
        }

        private void btnPrintPreview_Click(object sender, EventArgs e)
        {
            this.showPrintPreview();
        }

        public void showPrintPreview()
        {
            this.arrangeStickerMain();

            Cursor = Cursors.WaitCursor;
            PrintDocumentForm form = new PrintDocumentForm(pnlOutput);
            Cursor = Cursors.Default;
            form.Show();
            form.Activate();
            // form.BringToFront();
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            DateTime selDate = this.monthCalendar1.SelectionStart;
            //this.monthCalendar1.SelectionRange.Start = ImageUtilities.StartOfWeek(this.monthCalendar1.SelectionStart, DayOfWeek.Sunday);
            //this.monthCalendar1.SelectionRange.End = ImageUtilities.StartOfWeek(this.monthCalendar1.SelectionStart, DayOfWeek.Saturday);
            this.monthCalendar1.SelectionRange = new SelectionRange(
                                    ImageUtilities.StartOfWeek(selDate, DayOfWeek.Sunday)
                                    , ImageUtilities.EndOfWeek(selDate, DayOfWeek.Saturday));
          
        }

        private void LoadOrderTreeView(DateTime startDt, DateTime endDt)
        {
            string path = Constants.BASE_PATH + Constants.DRAFT_DIR;
            string[] drives = Directory.GetDirectories(path);

            this.BildingOrderTreeView(drives, -1);
        }

        private void treeView1_AfterExpand(object sender, TreeViewEventArgs e)
        {
            try
            {
                TreeNode node = e.Node;
                string drivePath = node.Tag.ToString();

                string[] filePaths = Directory.GetFiles(drivePath, "*.png", SearchOption.AllDirectories);
                treeView1.Nodes[e.Node.Index].Nodes.Clear();
                this.BildingOrderTreeView(filePaths, e.Node.Index);

            } catch(Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void BildingOrderTreeView(string[] drives, int nodeIdx)
        {
           
            foreach (string drive in drives)
            {
                DriveInfo di = new DriveInfo(drive);
                int driveImage;

                switch (di.DriveType)    //set the drive's icon
                {
                    case DriveType.CDRom:
                        driveImage = 3;
                        break;
                    case DriveType.Network:
                        driveImage = 6;
                        break;
                    case DriveType.NoRootDirectory:
                        driveImage = 8;
                        break;
                    case DriveType.Unknown:
                        driveImage = 8;
                        break;
                    default:
                        driveImage = 2;
                        break;
                }

                TreeNode node = new TreeNode(Path.GetFileName(drive), driveImage, driveImage);
                node.Tag = drive;

                if (nodeIdx == -1)
                {
                    if (di.IsReady)
                    {
                        node.Nodes.Add("");
                    }

                    treeView1.Nodes.Add(node);                   
                }
                else
                {          
                   treeView1.Nodes[nodeIdx].Nodes.Add(node);              
                    
                }
            }
        }

    }
}
