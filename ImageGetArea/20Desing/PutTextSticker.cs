﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WrapCar._20Desing
{
     
    public partial class PutTextForm : Form
    {
         DesignForm designForm;

        public PutTextForm( DesignForm designForm)
        {
            InitializeComponent();
            this.designForm = designForm;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            // Show the font dialog.
            DialogResult result = fontDialog1.ShowDialog();

            // See if user pressed ok.
            if (result == DialogResult.OK)
            {
                FontFamily fontFamily = fontDialog1.Font.FontFamily;
                float fontSize = fontDialog1.Font.Size;
                FontStyle style = fontDialog1.Font.Style;

                // Create the Font object for the image text drawing.
                Font font = new Font(fontFamily, fontSize);
                txtInputMessage.Font = font;

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Bitmap bmp = this.convertTextToImage(txtInputMessage.Text, txtInputMessage.Font.FontFamily.Name, txtInputMessage.Font.Size);
            designForm.setTextImage(bmp);
            designForm.Validate();
            this.Close();

        }

        private Bitmap convertTextToImage(string txt, string fontname, float fontsize)
        {
            //creating bitmap image
            Bitmap bmp = new Bitmap(1, 1);

            //FromImage method creates a new Graphics from the specified Image.
            Graphics graphics = Graphics.FromImage(bmp);

            // Create the Font object for the image text drawing.
            Font font = new Font(fontname, fontsize);

            // Instantiating object of Bitmap image again with the correct size for the text and font.
            SizeF stringSize = graphics.MeasureString(txt, font);

            bmp = new Bitmap(bmp, (int)stringSize.Width, (int)stringSize.Height);
            graphics = Graphics.FromImage(bmp);

            /* It can also be a way
            bmp = new Bitmap(bmp, new Size((int)graphics.MeasureString(txt, font).Width, (int)graphics.MeasureString(txt, font).Height));*/

            //Draw Specified text with specified format 
            graphics.DrawString(txt, font, Brushes.Black, 0, 0);
            font.Dispose();
            graphics.Flush();
            graphics.Dispose();

           return bmp;     //return Bitmap Image 
        }

    }
}
