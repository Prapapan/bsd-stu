﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.IO;
using System.Threading;
using WrapCar._00Utilities;
using WrapCar_Connection;
using System.Globalization;

namespace WrapCar._20Desing
{

    public partial class DesignForm : Form
    {

        #region Declare variable
        static string DEFAULT_LEFT_SIDE = Constants.BASE_PATH + Constants.IMG_CAR_DIR + "\\1-Car-Left.png";
        static string DEFAULT_BACK_SIDE = Constants.BASE_PATH + Constants.IMG_CAR_DIR + "\\2-Car-Back.png";
        static string DEFAULT_RIGHT_SIDE = Constants.BASE_PATH + Constants.IMG_CAR_DIR + "\\3-Car-Right.png";
        static string DEFAULT_FRONT_SIDE = Constants.BASE_PATH + Constants.IMG_CAR_DIR + "\\4-Car-Font.png";
        static string DEFAULT_TOP_SIDE = Constants.BASE_PATH + Constants.IMG_CAR_DIR + "\\5-Car-Roof.png";

        static int STK_IMAGE_TYPE = 0; // Image Type
        static int STK_LINE_TYPE = 1;  // Line Type

        Dictionary<string, StickerDetection> mapDicutObj = new Dictionary<string, StickerDetection>();

        List<StickerCtrl> stkList = new List<StickerCtrl>();
        StickerCtrl focusStkCtrl;
        ImageList imageList;
        List<string> imageNameDB = new List<string>();

        StickerListForm stkListForm;
        ProgressBarForm frmPgBar;
     

        SolidBrush sbGray = new SolidBrush(Color.Gray);
        SolidBrush sbGreen = new SolidBrush(Color.LimeGreen);
        SolidBrush sbTemp;

        bool mDown;
        Point mPosition = new Point();

        int objWidth = 160;
        int objHeight = 70 ;

        int cropWidth = 0;
        int cropHeight = 0;
        int mX = 0;
        int mY = 0;
        int xP = 0;
        int yP = 0;
        string image_file = "";
        bool isStkFormShow= false;

        List<Color> colorOutout = new List<Color>();
        public List<Point> points = new List<Point>();
      

        #endregion

        public DesignForm()
        {
            InitializeComponent();
            txtWidthPx.Text = objWidth.ToString();
            txtHeightPx.Text = objHeight.ToString();
        }

        private void DesignForm_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;

            generateOrderID();
            displayHexColorCode();
            lblHiddOldColor.BackColor = btnChangeColor.BackColor;

            //Blinding Combobox Sticker Type from DB
            DataTable table = new DataTable();
            table.Columns.Add("TypeID", typeof(string));
            table.Columns.Add("TypeName", typeof(string));
            table.Columns.Add("UnitPrice", typeof(decimal));
            table.Rows.Add("1", "แบบธรรมดา", 1.50);
            table.Rows.Add("2", "แบบมัน", 2.00);
            table.Rows.Add("3", "แคปล่า", 3.00);

            cbxStickerType.DataSource = table;
            cbxStickerType.DisplayMember = "TypeName";
            cbxStickerType.ValueMember = "UnitPrice";
            txtUnitPrice.Text = cbxStickerType.SelectedValue.ToString();
        }

        private void btGetImg_Click(object sender, EventArgs e)
        {
            openImg.Title = "Insert an Image";
            openImg.FileName = "";
            openImg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            openImg.ShowDialog();

            if (openImg.ShowDialog() != DialogResult.Cancel)
            {
                image_file = openImg.FileName;
                layout.Image = Image.FromFile(image_file);
            }
        }

        #region Apply line detection
        private void btnApply_Click(object sender, EventArgs e)
        {
            if (layout.Image != null && focusStkCtrl != null)
            {
                layout.Invalidate();
                colorOutout = new List<Color>();
                detectedLine();
                boxStkRander.Image = ImageUtilities.createStickerCutOffPNG(focusStkCtrl.GetBitmap(), "" + focusStkCtrl.GetObjId());

                StickerCtrl stk = stkList[focusStkCtrl.GetObjId()];
                stk.SetWidthInInch(Double.Parse(txtWidthInc.Text));
                stk.SetHeightInInch(Double.Parse(txtHeightInc.Text));
                stk.SetAreaInInch(Double.Parse(txtStickerArea.Text));
                stk.SetItemPrice(Decimal.Parse(txtItemPrice.Text));
                stkList[focusStkCtrl.GetObjId()] = stk;

                this.calTotalPrice();
            }
            layout.Invalidate();
        }

        private void detectedLine()
        {
            //string outStr = "";
            if (layout.Image != null && focusStkCtrl != null)
            {
                    colorOutout = new List<Color>();
                    
                    objWidth = focusStkCtrl.GetBitmap().Width;
                    objHeight = focusStkCtrl.GetBitmap().Height;

                        // Determine which ratio to use
                        double screenRatioWidth = (double)layout.Image.Width / layout.Width;
                        double screenRatioHeight = (double)layout.Image.Height / layout.Height;

                        int targetEndWidth = xP + objWidth;
                        int targetEndHeight = yP + objHeight;

                        if (targetEndWidth > layout.Image.Width)
                        {
                            targetEndWidth = layout.Image.Width;
                            cropWidth = targetEndWidth - xP;
                        }
                        else
                            cropWidth = objWidth;

                        if (xP < 0) xP = 0;
                        if (yP < 0) yP = 0;

                        //  outStr += "Start x: " + xP + " Start y: " + yP + " Ratio: " + screenRatioWidth + "," + screenRatioHeight + "\n";
                        //  outStr += "Width: " + targetEndWidth + " Height: " + targetEndHeight + "\n";

                        if (targetEndHeight > layout.Image.Height)
                        {
                            targetEndHeight = layout.Image.Height;
                            cropHeight = targetEndHeight - yP;
                        }
                        else
                        {
                            cropHeight = objHeight;
                        }

                        Bitmap image = new Bitmap(layout.Image);
                        Bitmap newCarBitmap = new Bitmap(objWidth, objHeight);
                        Bitmap newSticker = focusStkCtrl.GetOriginalBitmap();
                        List<Point> pointList = new List<Point>();
                        List<double> lineDetectPointList = new List<double>();

                        for (int i = xP; i < targetEndWidth; i++)
                        {
                            int ii = (int)Math.Round(i * screenRatioWidth);

                            for (int j = yP; j < targetEndHeight; j++)
                            {
                                int jj = (int)Math.Round(j * screenRatioHeight);

                                //Get color from the image
                                Color color = image.GetPixel(ii, jj);
                                string htmlHexColorValueThree = String.Format("{0:X2}{1:X2}{2:X2}", color.R, color.G, color.B);
                                colorOutout.Add(color);
                              
                                //Detect only black line
                                if (color.R >= 0 && color.R <= 200)
                                {
                                    //Plot new car structure that got from detection
                                    newCarBitmap.SetPixel(i - xP, j - yP, color); 

                                    //Remove sticker line (Cutoff)
                                    newSticker.SetPixel(i - xP, j - yP, Color.Transparent);

                                    //Corrected Line Point in image for draw border Bezier Curve on sticker.
                                    lineDetectPointList.Add((double)i - xP);
                                    lineDetectPointList.Add((double)j - yP);
                                }
                                else
                                {
                                    //Corrected Point in image for crop area (fitting image)
                                    pointList.Add(new Point(i - xP, j - yP));
                                }

                            }
                        }

                        int minX = pointList.Min(m => m.X);
                        int minY = pointList.Min(m => m.Y);
                        int maxX = pointList.Max(m => m.X);
                        int maxY = pointList.Max(m => m.Y);
                        int width = maxX - minX;
                        int height = maxY - minY;

                        // Crop border to fitting image
                        Rectangle selection = new Rectangle(minX, minY, width, height);
                        Bitmap cropFitting = newSticker.Clone(selection, newSticker.PixelFormat);
                        focusStkCtrl.SetBitmap(newSticker);
                        boxImgRander.Image = ImageUtilities.createCarCutOffPNG(newCarBitmap, focusStkCtrl.GetObjId().ToString());

            }
        }

        private void extractImageByDicut(Bitmap newSticker)
        {
            
            StickerDetection stkDtc = null;
            List<Point> pointList = null;

            int Width = newSticker.Width;
            int Height = newSticker.Height;

            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                   //Get color from the image
                    Color color = newSticker.GetPixel(i, j);

                    if (Color.Transparent != color)
                    {
                        string htmlHexColorValueThree = String.Format("{0:X2}{1:X2}{2:X2}", color.R, color.G, color.B);

                        Color nearestColor = ImageUtilities.findTheNearestColor(mapDicutObj, color);
                        string htmlHexNearestColor = String.Format("{0:X2}{1:X2}{2:X2}", nearestColor.R, nearestColor.G, nearestColor.B);

                        if (!mapDicutObj.ContainsKey(htmlHexNearestColor))
                        {
                            stkDtc = new StickerDetection();
                            stkDtc.colorHex = htmlHexColorValueThree;
                            stkDtc.color = color;
                            Bitmap bmpTmp = new Bitmap(Width, Height);
                            using (Graphics g = Graphics.FromImage(bmpTmp))
                            {
                                g.Clear(Color.FromArgb(color.R, color.G, color.B));
                            }
                            stkDtc.imgBmp = bmpTmp;
                            stkDtc.pointList = new List<Point>();
                            mapDicutObj.Add(htmlHexNearestColor, stkDtc);
                            //Console.WriteLine(">>>> nerestColor=" + htmlHexNearestColor + " realColor=" + stkDtc.colorHex);

                        }

                        //Geting object from DataDictionary (Separate by color)
                        stkDtc = mapDicutObj[htmlHexNearestColor];
                        Bitmap extractImageColor = stkDtc.imgBmp;

                        pointList = stkDtc.pointList;

                        //Grouping the same color pixel for randering a new image
                        //Make to grayscale
                        int luma = (int)(color.R * 0.3 + color.G * 0.59 + color.B * 0.11);
                        extractImageColor.SetPixel(i, j, Color.White);// Color.FromArgb(luma,luma,luma));

                        //Corrected Point in image for crop area (fitting image)
                        pointList.Add(new Point(i, j));

                        //Set back to the DataDictionary (Separate by color)
                        stkDtc.imgBmp = extractImageColor;
                        stkDtc.pointList = pointList;
                        mapDicutObj[htmlHexNearestColor] = stkDtc;
                    }
                }
            }

            //Save new image the separated by color to PNG
            int number = 1;
            int basePointArea = mapDicutObj.Values.Max(t => t.pointList.Count)/6;
            List<string> removeKey = new List<string>();

            foreach (string key in mapDicutObj.Keys)
            {
                StickerDetection stkDet = mapDicutObj[key];
                //newSticker = this.ReplaceColor(newSticker, stkDet.color, stkDet.color);
                if (stkDet.pointList.Count > basePointArea)
                {
                    ImageUtilities.createStickerCutOffPNG(stkDet.imgBmp, focusStkCtrl.GetObjId() + "_" + number);
                }
                else
                {
                    removeKey.Add(key);
                }
                number++;
            }

            foreach (string key in removeKey)
            {
                mapDicutObj.Remove(key);
            }

        }

        #endregion

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            objWidth = Int16.Parse(txtWidthPx.Text);
        }

        private void txtHeight_TextChanged(object sender, EventArgs e)
        {
            objHeight = Int16.Parse(txtHeightPx.Text);
        }

        #region Layout mouse event

        private void layout_MouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {
                mDown = true;
                mPosition = new Point(e.X, e.Y);
                sbTemp = sbGray;
                layout.Invalidate();
            }
            else
            {
                points.Clear();
                sbTemp = null;
                layout.Invalidate();
            }
        }

        private void layout_MouseMove(object sender, MouseEventArgs e)
        {

            if (mDown)
            {
                mPosition = new Point(e.X, e.Y);
                sbTemp = sbGray;
                layout.Invalidate();
            }
            if (focusStkCtrl != null)
            {          
                //focusStkCtrl.SwitchToOriginlBitmap();
                displayStickerTextInfo();
           }
        }

        private void layout_MouseUp(object sender, MouseEventArgs e)
        {
            if (isStkFormShow)
            {
                stkListForm.Hide();
                isStkFormShow = false;
            }

                if (e.Button == MouseButtons.Left)
                {
                    mPosition = new Point(e.X, e.Y);
                    points.Add(mPosition);
                    sbTemp = sbGreen;
                    layout.Invalidate();
                    mDown = false;

                }
                if (focusStkCtrl != null)
                {
                     displayStickerTextInfo();                  
                }
        }

        public void displayStickerTextInfo()
        {
            int widthPixel = focusStkCtrl.GetBitmap().Width;
            int heighPixel = focusStkCtrl.GetBitmap().Height;

            //Pixel format
            txtWidthPx.Text = "" + widthPixel;
            txtHeightPx.Text = "" + heighPixel;

            double widthInc = convertPixelToInch(widthPixel);
            double heightInc = convertPixelToInch(heighPixel);
            //Inch format
            txtWidthInc.Text = "" + widthInc;
            txtHeightInc.Text = "" + heightInc;

            txtStickerArea.Text = "" + calculateStickerArea(widthInc, heightInc);
        }

        public double convertPixelToInch(double valPixel)
        {
            double ratio = Double.Parse(txtRatio.Text);

            return Math.Round((valPixel * ratio), 2);
        }

        public double calculateStickerArea(double valWidthInc, double valHeightInc)
        {
            return Math.Round((valWidthInc * valHeightInc), 2);
        }

        public void Draw(Graphics g)
        {
            g.DrawImage((Image)focusStkCtrl.GetBitmap(), focusStkCtrl.GetRectangle());

            foreach (StickerCtrl.PosSizableRect pos in Enum.GetValues(typeof(StickerCtrl.PosSizableRect)))
            {
                g.DrawRectangle(new Pen(Color.Gray), focusStkCtrl.GetRect(pos));

            }
        }


        private void layout_Paint(object sender, PaintEventArgs e)
        {
            mX = objWidth / 2;
            mY = objHeight / 2;

            if (image_file != "")
            {
                layout.Image = Image.FromFile(image_file);
            }

            if (focusStkCtrl != null)
            {
                focusStkCtrl.SetPictureBox(this.layout, this);
               //Draw(e.Graphics);

                xP = focusStkCtrl.GetLeftTopNodeSelectable().X;
                yP = focusStkCtrl.GetLeftTopNodeSelectable().Y;

                
               // e.Graphics.DrawImage(focusStkCtrl.GetBitmap(), new Point(xP, yP));

            }
            else
            {
                xP = mPosition.X - mX;
                yP = mPosition.Y - mY;
            }

            //Console.WriteLine(">>>>>>>>>>>>>>>> [mPosition.X,mPosition.Y] : [" + mPosition.X + "," + mPosition.Y + "]");
            //Console.WriteLine(">>>>>>>>>>>>>>>> [xP,yP] : [" + xP + "," + yP + "]");
        }

   
        #endregion

        private void newStickerListForm(int stkType)
        {

            // Creation of the first ImageListPopup
            stkListForm = new StickerListForm(stkType);
            stkListForm.EnableDragDrop = true;
            stkListForm.Init(imageList, 0, 0, 5, 3);
            stkListForm.ItemClick += new StickerListLoadingEventHandler(OnItemClicked);
        }

        private void OnItemClicked(object sender, StickerListLoadingEventArgs e)
        {

            if (sender.Equals(stkListForm))
            {
                Bitmap selectedItem = (Bitmap)imageList.Images[e.SelectedItem];
                string stkName = imageNameDB[e.SelectedItem];
                txtStickerName.Text = " " + stkName;

                StickerCtrl stkCtl = createStickerCtrl(selectedItem);
                stkList.Add(stkCtl);
                getFocusSticker(stkCtl.GetObjId());
            }
        }

        private void btnStkImage_Click(object sender, EventArgs e)
        {
            bool isNew = false;
            if (stkListForm == null || !stkListForm.isImageType())
            {
                isNew = true;
                if (stkListForm != null)
                {
                    stkListForm.Dispose();
                    stkListForm = null;
                }
            }

            if (isNew)
            {
                string path = Constants.BASE_PATH + Constants.IMG_STICKER_DIR;
                initialStickerList(path, 120, 50);
                newStickerListForm(STK_IMAGE_TYPE);
            }

            stkListForm.Show(125, 120);
            isStkFormShow = true;
           

        }

        private void btnStkLine_Click(object sender, EventArgs e)
        {
            bool isNew = false;
             if (stkListForm == null || !stkListForm.isLineType())
            {
                isNew = true;
                if (stkListForm != null)
                {
                    stkListForm.Dispose();
                    stkListForm = null;
                }
            }

            if (isNew)
            {
                string path = Constants.BASE_PATH + Constants.LINE_STICKER_DIR;
                initialStickerList(path, 170, 50);
                newStickerListForm(STK_LINE_TYPE);
            }

            stkListForm.Show(125, 120);
            isStkFormShow = true;

        }


        private void btnStkText_Click(object sender, EventArgs e)
        {
            // Creation of the first PutTextForm Popup
            PutTextForm textForm = new PutTextForm(this);
            textForm.Location = new Point(125, 120);
            textForm.Show();
        }

        private void initialStickerList(string path, int layoutWidth, int layoutHeight)
        {

            // Creation of the First ImageList
            imageList = new ImageList();
            imageList.ImageSize = new Size(layoutWidth, layoutHeight);
            imageList.ColorDepth = ColorDepth.Depth32Bit;
            imageList.TransparentColor = Color.FromArgb(255, 0, 255);

            string[] filePaths = Directory.GetFiles(path, "*.png", SearchOption.AllDirectories);

            //Mockup Sticker Data
            //int i = 1;
            foreach(string filePath in filePaths)
            {
                imageList.Images.Add(ImageUtilities.ResizeImage(Image.FromFile(filePath), objWidth, objHeight, false));
                imageNameDB.Add(Path.GetFileNameWithoutExtension(filePath));
            }
        }

        private StickerCtrl createStickerCtrl(Bitmap stkBmp)
        {
            int idx = stkList.Count;
            return addStickerCtrl(stkBmp, idx);
        }

        private StickerCtrl addStickerCtrl(Bitmap stkBmp, int idx)
        {
            Rectangle rect = new Rectangle(new Point(layout.Top, layout.Left), new Size(stkBmp.Width, stkBmp.Height));

            string stkPath = ImageUtilities.getOrderFolderPath() + ImageUtilities.generateStickerPNGName("" + idx);
            string name = Path.GetFileNameWithoutExtension(stkPath);

            StickerCtrl stkCtl = new StickerCtrl(idx, rect, stkBmp, name, stkPath);
            return stkCtl;
        }

        #region change car side
        public void getFocusSticker(int idx)
        {
            focusStkCtrl = stkList[idx];
        }

        private void resetCarToOriginalColor()
        {
            string htmlHexColorValue = String.Format("{0:X2}{1:X2}{2:X2}", Color.White.R, Color.White.G, Color.White.B);
            lblHiddOldColor.BackColor = Color.White;
        }

        private void btnTopSide_Click(object sender, EventArgs e)
        {
            layout.Image = Image.FromFile(DEFAULT_TOP_SIDE);
            this.resetCarToOriginalColor();
            this.changeCarColor();

        }

        private void btnLeftSide_Click(object sender, EventArgs e)
        {
            layout.Image = Image.FromFile(DEFAULT_LEFT_SIDE);
            this.resetCarToOriginalColor();
            this.changeCarColor();
        }

        private void btnFrontSide_Click(object sender, EventArgs e)
        {
            layout.Image = Image.FromFile(DEFAULT_FRONT_SIDE);
            this.resetCarToOriginalColor();
            this.changeCarColor();
        }

        private void btnRightSide_Click(object sender, EventArgs e)
        {
            layout.Image = Image.FromFile(DEFAULT_RIGHT_SIDE);
            this.resetCarToOriginalColor();
            this.changeCarColor();
        }

        private void btnBackSide_Click(object sender, EventArgs e)
        {
            layout.Image = Image.FromFile(DEFAULT_BACK_SIDE);
            this.resetCarToOriginalColor();
            this.changeCarColor();
        }
        #endregion

        private void btnChangeColor_Click(object sender, EventArgs e)
        {

            // Show the color dialog.
            DialogResult result = colorDialog1.ShowDialog();
            // See if user pressed ok.
            if (result == DialogResult.OK)
            {
                // Set form background to the selected color.
                Color selectColor = colorDialog1.Color;
                btnChangeColor.BackColor = selectColor;
                this.changeCarColor();
            }
        }

        private void btnChangeColor_BackColorChanged(object sender, EventArgs e)
        {
            displayHexColorCode();
        }

        private void displayHexColorCode()
        {
            Color selectColor = btnChangeColor.BackColor;
            string htmlHexColorValue = String.Format("{0:X2}{1:X2}{2:X2}", selectColor.R, selectColor.G, selectColor.B);
            lblColorCode.Text = "#" + htmlHexColorValue;
        }

        private void startProgressBar()
        {  //Start progress bar.
            frmPgBar = new ProgressBarForm();
            //frmPgBar.SetProgress(0);
            frmPgBar.ShowDialog();
        }

        private void changeCarColor()
        {

            if (layout.Image != null)
            {

                Thread progressThrd = new Thread(new ThreadStart(this.startProgressBar));
                progressThrd.Start();
              
                int targetEndWidth = layout.Location.X + layout.Width;
                int targetEndHeight = layout.Location.Y + layout.Height;

                if (targetEndWidth > layout.Width)
                {
                    targetEndWidth = layout.Width;
                }

                //  outStr += "Start x: " + xP + " Start y: " + yP + " Ratio: " + screenRatioWidth + "," + screenRatioHeight + "\n";
                //  outStr += "Width: " + targetEndWidth + " Height: " + targetEndHeight + "\n";

                if (targetEndHeight > layout.Height)
                {
                    targetEndHeight = layout.Height;
                }

                Bitmap image = new Bitmap(layout.Image);
                Bitmap newBitmap = new Bitmap(layout.Image.Width, layout.Image.Height);
                Color newCarColor = btnChangeColor.BackColor;
                Color oldCarColor = lblHiddOldColor.BackColor;
             
                for (int i =0; i < targetEndWidth; i++)
                {
                   // int ii = (int)Math.Round(i * screenRatioWidth);
                   
                    for (int j = 0; j < targetEndHeight; j++)
                    {
                    //    int jj = (int)Math.Round(j * screenRatioHeight);

                        Color x = image.GetPixel(i, j);
                        string htmlHexColorValueThree = String.Format("{0:X2}{1:X2}{2:X2}", x.R, x.G, x.B);
                      
                        //outStr += "[ (" + ii + ", " + jj + ") : #" + htmlHexColorValueThree + " ]\n";

                        //Detect only body car color
                        if (   (oldCarColor.R == x.R) 
                            && (oldCarColor.G == x.G)
                            && (oldCarColor.B == x.B))
                        {
                            newBitmap.SetPixel(i, j, newCarColor); //Change new color

                        }
                        else
                        {
                            newBitmap.SetPixel(i , j, x); //Change new color
                        }
                    }
                }

                layout.Image = newBitmap;
                lblHiddOldColor.BackColor = btnChangeColor.BackColor;
                Thread.Sleep(100);
                progressThrd.Abort();
            }
        }

        public void setTextImage(Bitmap txtImg)
        {

            StickerCtrl stkCtl = createStickerCtrl(txtImg);
            stkList.Add(stkCtl);
            getFocusSticker(stkCtl.GetObjId());
            layout.Invalidate();     
        }

        #region Generate Naming 
        private void generateOrderID()
        {
            // Generate orderID
            Constants.orderID = "ORD_" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day;
        }

        public int getCurrentIdx()
        {
           return this.focusStkCtrl.GetObjId();
        }
   
        #endregion

        private void btnConfirmOrder_Click(object sender, EventArgs e)
        {

        }

        private void cbxStickerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            txtUnitPrice.Text = cmb.SelectedValue.ToString();
        }

        private void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            double totally = calTotally(txtUnitPrice.Text, txtStickerArea.Text); ;
            txtItemPrice.Text = totally.ToString();
        }

        private void txtStickerArea_TextChanged(object sender, EventArgs e)
        {
            double totally = calTotally(txtUnitPrice.Text , txtStickerArea.Text); ;
            txtItemPrice.Text = totally.ToString();
            
        }

        private double calTotally(string txtUnitPrice, string txtStickerArea)
        {
            double unitPrice = 0;
            double stickerArea = 0;
            try
            {
                unitPrice = Double.Parse(txtUnitPrice);
            }
            catch (Exception ex)
            {
                unitPrice = 0.0d;
            }

            try
            {
                stickerArea = Double.Parse(txtStickerArea);
            }
            catch (Exception ex)
            {
                stickerArea = 0.0d;
            }

            return this.calItemPrice(unitPrice, stickerArea);
        }

        private double calItemPrice(double dUnitPrice, double dStickerArea)
        {
            return Math.Round(dUnitPrice * dStickerArea, 2);
        }

        private decimal calTotalPrice()
        {

            decimal totallyAll = stkList.Sum(t => t.GetItemPrice());
            StickerCtrl[] arr1 = stkList.ToArray();
            StickerCtrl[] arr2 = {focusStkCtrl};

            //if (arr1 != null && arr1.Length > 0)
            //{
            //    List<StickerCtrl> temp = (List<StickerCtrl>)arr1.Except(arr2);
            //    if (temp != null && temp.Count > 0)
            //    {
                   // totallyAll = temp.Sum(t => t.GetItemPrice()) + Decimal.Parse(txtItemPrice.Text);
                    txtTotalAll.Text = totallyAll.ToString();
            //    }
            //}
            return totallyAll;
        }


        private void txtItemPrice_TextChanged(object sender, EventArgs e)
        {
           // this.calTotalPrice();
        }

        private void btnBluePrint_Click(object sender, EventArgs e)
        {
           
        }

        private void ArrangeStickerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ArrangeStickerForm arrg = new ArrangeStickerForm(mapDicutObj);
            arrg.showPrintPreview();
           
            //arrg.Show();
        }

    }

    public class StickerDetection
    {
        public string colorHex { get; set; }
        public Color color { get; set; }
        public Bitmap imgBmp { get; set; }
        public List<Point> pointList { get; set; }

    }

   
}