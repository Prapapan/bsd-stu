﻿namespace WrapCar.indy
{
    partial class testform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.customername = new System.Windows.Forms.Label();
            this.tbcustomername = new System.Windows.Forms.TextBox();
            this.btnsearch = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.customerDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cUSIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tITLEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cUSFNAMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cUSLNAMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mOBILENODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eMAILDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDCARDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDCARDISSUEDATEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDCARDEXPDATEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dRIVINGLICENCEIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dRIVINGLICENCEISSUEDATEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dRIVINGLICENCEEXPDATEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rEGISTERDATEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customerDataBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // customername
            // 
            this.customername.AutoSize = true;
            this.customername.Location = new System.Drawing.Point(24, 47);
            this.customername.Name = "customername";
            this.customername.Size = new System.Drawing.Size(65, 13);
            this.customername.TabIndex = 0;
            this.customername.Text = "ชื่อลูกค้าแจร้";
            // 
            // tbcustomername
            // 
            this.tbcustomername.Location = new System.Drawing.Point(121, 39);
            this.tbcustomername.Name = "tbcustomername";
            this.tbcustomername.Size = new System.Drawing.Size(100, 20);
            this.tbcustomername.TabIndex = 1;
            // 
            // btnsearch
            // 
            this.btnsearch.Location = new System.Drawing.Point(241, 39);
            this.btnsearch.Name = "btnsearch";
            this.btnsearch.Size = new System.Drawing.Size(75, 23);
            this.btnsearch.TabIndex = 2;
            this.btnsearch.Text = "ค้นหา";
            this.btnsearch.UseVisualStyleBackColor = true;
            this.btnsearch.Click += new System.EventHandler(this.btnsearch_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cUSIDDataGridViewTextBoxColumn,
            this.tITLEDataGridViewTextBoxColumn,
            this.cUSFNAMEDataGridViewTextBoxColumn,
            this.cUSLNAMEDataGridViewTextBoxColumn,
            this.mOBILENODataGridViewTextBoxColumn,
            this.eMAILDataGridViewTextBoxColumn,
            this.iDCARDDataGridViewTextBoxColumn,
            this.iDCARDISSUEDATEDataGridViewTextBoxColumn,
            this.iDCARDEXPDATEDataGridViewTextBoxColumn,
            this.dRIVINGLICENCEIDDataGridViewTextBoxColumn,
            this.dRIVINGLICENCEISSUEDATEDataGridViewTextBoxColumn,
            this.dRIVINGLICENCEEXPDATEDataGridViewTextBoxColumn,
            this.rEGISTERDATEDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.customerDataBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 105);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(728, 150);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // customerDataBindingSource
            // 
            this.customerDataBindingSource.DataSource = typeof(WrapCar_DataModel.CustomerData);
            // 
            // cUSIDDataGridViewTextBoxColumn
            // 
            this.cUSIDDataGridViewTextBoxColumn.DataPropertyName = "CUS_ID";
            this.cUSIDDataGridViewTextBoxColumn.HeaderText = "CUS_ID";
            this.cUSIDDataGridViewTextBoxColumn.Name = "cUSIDDataGridViewTextBoxColumn";
            // 
            // tITLEDataGridViewTextBoxColumn
            // 
            this.tITLEDataGridViewTextBoxColumn.DataPropertyName = "TITLE";
            this.tITLEDataGridViewTextBoxColumn.HeaderText = "TITLE";
            this.tITLEDataGridViewTextBoxColumn.Name = "tITLEDataGridViewTextBoxColumn";
            // 
            // cUSFNAMEDataGridViewTextBoxColumn
            // 
            this.cUSFNAMEDataGridViewTextBoxColumn.DataPropertyName = "CUS_FNAME";
            this.cUSFNAMEDataGridViewTextBoxColumn.HeaderText = "CUS_FNAME";
            this.cUSFNAMEDataGridViewTextBoxColumn.Name = "cUSFNAMEDataGridViewTextBoxColumn";
            // 
            // cUSLNAMEDataGridViewTextBoxColumn
            // 
            this.cUSLNAMEDataGridViewTextBoxColumn.DataPropertyName = "CUS_LNAME";
            this.cUSLNAMEDataGridViewTextBoxColumn.HeaderText = "CUS_LNAME";
            this.cUSLNAMEDataGridViewTextBoxColumn.Name = "cUSLNAMEDataGridViewTextBoxColumn";
            // 
            // mOBILENODataGridViewTextBoxColumn
            // 
            this.mOBILENODataGridViewTextBoxColumn.DataPropertyName = "MOBILE_NO";
            this.mOBILENODataGridViewTextBoxColumn.HeaderText = "MOBILE_NO";
            this.mOBILENODataGridViewTextBoxColumn.Name = "mOBILENODataGridViewTextBoxColumn";
            // 
            // eMAILDataGridViewTextBoxColumn
            // 
            this.eMAILDataGridViewTextBoxColumn.DataPropertyName = "EMAIL";
            this.eMAILDataGridViewTextBoxColumn.HeaderText = "EMAIL";
            this.eMAILDataGridViewTextBoxColumn.Name = "eMAILDataGridViewTextBoxColumn";
            // 
            // iDCARDDataGridViewTextBoxColumn
            // 
            this.iDCARDDataGridViewTextBoxColumn.DataPropertyName = "ID_CARD";
            this.iDCARDDataGridViewTextBoxColumn.HeaderText = "ID_CARD";
            this.iDCARDDataGridViewTextBoxColumn.Name = "iDCARDDataGridViewTextBoxColumn";
            // 
            // iDCARDISSUEDATEDataGridViewTextBoxColumn
            // 
            this.iDCARDISSUEDATEDataGridViewTextBoxColumn.DataPropertyName = "ID_CARD_ISSUE_DATE";
            this.iDCARDISSUEDATEDataGridViewTextBoxColumn.HeaderText = "ID_CARD_ISSUE_DATE";
            this.iDCARDISSUEDATEDataGridViewTextBoxColumn.Name = "iDCARDISSUEDATEDataGridViewTextBoxColumn";
            // 
            // iDCARDEXPDATEDataGridViewTextBoxColumn
            // 
            this.iDCARDEXPDATEDataGridViewTextBoxColumn.DataPropertyName = "ID_CARD_EXP_DATE";
            this.iDCARDEXPDATEDataGridViewTextBoxColumn.HeaderText = "ID_CARD_EXP_DATE";
            this.iDCARDEXPDATEDataGridViewTextBoxColumn.Name = "iDCARDEXPDATEDataGridViewTextBoxColumn";
            // 
            // dRIVINGLICENCEIDDataGridViewTextBoxColumn
            // 
            this.dRIVINGLICENCEIDDataGridViewTextBoxColumn.DataPropertyName = "DRIVING_LICENCE_ID";
            this.dRIVINGLICENCEIDDataGridViewTextBoxColumn.HeaderText = "DRIVING_LICENCE_ID";
            this.dRIVINGLICENCEIDDataGridViewTextBoxColumn.Name = "dRIVINGLICENCEIDDataGridViewTextBoxColumn";
            // 
            // dRIVINGLICENCEISSUEDATEDataGridViewTextBoxColumn
            // 
            this.dRIVINGLICENCEISSUEDATEDataGridViewTextBoxColumn.DataPropertyName = "DRIVING_LICENCE_ISSUE_DATE";
            this.dRIVINGLICENCEISSUEDATEDataGridViewTextBoxColumn.HeaderText = "DRIVING_LICENCE_ISSUE_DATE";
            this.dRIVINGLICENCEISSUEDATEDataGridViewTextBoxColumn.Name = "dRIVINGLICENCEISSUEDATEDataGridViewTextBoxColumn";
            // 
            // dRIVINGLICENCEEXPDATEDataGridViewTextBoxColumn
            // 
            this.dRIVINGLICENCEEXPDATEDataGridViewTextBoxColumn.DataPropertyName = "DRIVING_LICENCE_EXP_DATE";
            this.dRIVINGLICENCEEXPDATEDataGridViewTextBoxColumn.HeaderText = "DRIVING_LICENCE_EXP_DATE";
            this.dRIVINGLICENCEEXPDATEDataGridViewTextBoxColumn.Name = "dRIVINGLICENCEEXPDATEDataGridViewTextBoxColumn";
            // 
            // rEGISTERDATEDataGridViewTextBoxColumn
            // 
            this.rEGISTERDATEDataGridViewTextBoxColumn.DataPropertyName = "REGISTER_DATE";
            this.rEGISTERDATEDataGridViewTextBoxColumn.HeaderText = "REGISTER_DATE";
            this.rEGISTERDATEDataGridViewTextBoxColumn.Name = "rEGISTERDATEDataGridViewTextBoxColumn";
            // 
            // testform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 729);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnsearch);
            this.Controls.Add(this.tbcustomername);
            this.Controls.Add(this.customername);
            this.Name = "testform";
            this.Text = "testform";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customerDataBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label customername;
        private System.Windows.Forms.TextBox tbcustomername;
        private System.Windows.Forms.Button btnsearch;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cUSIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tITLEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cUSFNAMEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cUSLNAMEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mOBILENODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eMAILDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDCARDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDCARDISSUEDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDCARDEXPDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dRIVINGLICENCEIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dRIVINGLICENCEISSUEDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dRIVINGLICENCEEXPDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rEGISTERDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource customerDataBindingSource;
    }
}