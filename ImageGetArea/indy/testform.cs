﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;

namespace WrapCar.indy
{
    public partial class testform : Form
    {
        public testform()
        {
            InitializeComponent();
        }

        private void btnsearch_Click(object sender, EventArgs e)
        {
             CustomerDAO dao = new CustomerDAO();
            List<CustomerData> custDataList = new List<CustomerData>();

            custDataList = dao.getCustomerList();

            int row = 0;
            int col = 0;

            dataGridView1.ColumnCount = 3;
            dataGridView1.Columns[0].Name = "No.";
            dataGridView1.Columns[1].Name = "Customer ID";
            dataGridView1.Columns[2].Name = "Customer Name";

            foreach (CustomerData custData in custDataList)
            {
                string[] dataRow = new string[] { "" + row++, custData.CUS_ID, custData.CUS_FNAME + " " + custData.CUS_LNAME };
                dataGridView1.Rows.Add(dataRow);
            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
