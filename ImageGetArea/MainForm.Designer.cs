﻿namespace ImageGetArea
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabPageDesign = new System.Windows.Forms.TabPage();
            this.tabPageAppointment = new System.Windows.Forms.TabPage();
            this.tabMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabPageDesign);
            this.tabMain.Controls.Add(this.tabPageAppointment);
            this.tabMain.Location = new System.Drawing.Point(0, -1);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(1152, 730);
            this.tabMain.TabIndex = 4;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabDesign_SelectedIndexChanged);
            // 
            // tabPageDesign
            // 
            this.tabPageDesign.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tabPageDesign.Location = new System.Drawing.Point(4, 22);
            this.tabPageDesign.Name = "tabPageDesign";
            this.tabPageDesign.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDesign.Size = new System.Drawing.Size(1144, 704);
            this.tabPageDesign.TabIndex = 0;
            this.tabPageDesign.Text = "ออกแบบสติ๊กเกอร์ติดรถ";
            this.tabPageDesign.UseVisualStyleBackColor = true;
            // 
            // tabPageAppointment
            // 
            this.tabPageAppointment.Location = new System.Drawing.Point(4, 22);
            this.tabPageAppointment.Name = "tabPageAppointment";
            this.tabPageAppointment.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAppointment.Size = new System.Drawing.Size(1144, 704);
            this.tabPageAppointment.TabIndex = 1;
            this.tabPageAppointment.Text = "ตารางการนัดหมาย";
            this.tabPageAppointment.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1152, 730);
            this.Controls.Add(this.tabMain);
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Design Screen";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tabMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabPageDesign;
        private System.Windows.Forms.TabPage tabPageAppointment;
    }
}