﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;


namespace WrapCar._10Customer
{
    public partial class CustomerForm : Form
    {
        public CustomerForm()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            CustomerDAO dao = new CustomerDAO();
            List<CustomerData> custDataList = new List<CustomerData>();

            custDataList = dao.getCustomerList();
            int row = 0;

            dataGridView1.ColumnCount = 3;
            dataGridView1.Columns[0].Name = "No.";
            dataGridView1.Columns[1].Name = "Customer ID";
            dataGridView1.Columns[2].Name = "Customer Name";
          
            if(txtCustName.Text != "")
               custDataList = custDataList.Where(cus => cus.CUS_FNAME.Contains(txtCustName.Text)).ToList();

            dataGridView1.Rows.Clear();
            foreach(CustomerData custData in custDataList){
                string[] dataRow = new string[] { "" + (++row)
                                                , custData.CUS_ID.ToString()
                                                , custData.CUS_FNAME + " " + custData.CUS_LNAME };
                dataGridView1.Rows.Add(dataRow);             
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            RegisterCustomerForm form = new RegisterCustomerForm();
            form.Show();
        }
    }
}
