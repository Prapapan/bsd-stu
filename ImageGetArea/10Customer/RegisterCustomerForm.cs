﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WrapCar_Connection;
using WrapCar_DataModel;

namespace WrapCar._10Customer
{
    public partial class RegisterCustomerForm : Form
    {
        public RegisterCustomerForm()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if ((string.Empty == txtName.Text) 
                || (string.Empty == txtSurname.Text)
                || (string.Empty == txtTelNo.Text)
                || (string.Empty == txtEmail.Text))
            {
                MessageBox.Show("กรุณาระบุ ชื่อ นามสกุล เบอร์โทรศัพท์ และอีเมลล์ให้ครบถ้วน");
            }
            else
            {
                TB_M_CUSTOMER data = new TB_M_CUSTOMER();
                data.TITLE = cmbTitle.SelectedItem.ToString();
                data.CUS_FNAME = txtName.Text;
                data.CUS_LNAME = txtSurname.Text;
                data.EMAIL = (string.Empty == txtEmail.Text) ? null : txtEmail.Text;
                data.MOBILE_NO = (string.Empty == txtTelNo.Text) ? null : txtTelNo.Text;
                data.REGISTER_DATE = DateTime.Now;

                CustomerDAO dao = new CustomerDAO();
                bool ret = dao.saveCustomer(data);
                if (ret)
                {
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ");
                }
                else
                {
                    MessageBox.Show("บันทึกข้อมูลไม่สำเร็จ กรุณาตรวจสอบใหม่");
                }
            }
        }
    }
}
