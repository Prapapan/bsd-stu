//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WrapCar_Connection
{
    using System;
    using System.Collections.Generic;
    
    public partial class TB_M_STICKER_ITEM
    {
        public TB_M_STICKER_ITEM()
        {
            this.TB_R_ORDER_D = new HashSet<TB_R_ORDER_D>();
        }
    
        public string STK_ITEM_ID { get; set; }
        public string STK_CATEGORY_ID { get; set; }
        public string STK_NAME { get; set; }
        public string STK_PATH { get; set; }
        public Nullable<decimal> WIDTH { get; set; }
        public Nullable<decimal> HEIGHT { get; set; }
        public Nullable<decimal> MH_RATE { get; set; }
    
        public virtual TB_M_STICKER_CATEGORY TB_M_STICKER_CATEGORY { get; set; }
        public virtual ICollection<TB_R_ORDER_D> TB_R_ORDER_D { get; set; }
    }
}
