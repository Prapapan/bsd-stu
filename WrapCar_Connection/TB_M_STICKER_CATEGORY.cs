//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WrapCar_Connection
{
    using System;
    using System.Collections.Generic;
    
    public partial class TB_M_STICKER_CATEGORY
    {
        public TB_M_STICKER_CATEGORY()
        {
            this.TB_M_STICKER_ITEM = new HashSet<TB_M_STICKER_ITEM>();
        }
    
        public string STK_CATEGORY_ID { get; set; }
        public string STK_CATEGORY_NAME { get; set; }
    
        public virtual ICollection<TB_M_STICKER_ITEM> TB_M_STICKER_ITEM { get; set; }
    }
}
