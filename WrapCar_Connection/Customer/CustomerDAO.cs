﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WrapCar_DataModel;

namespace WrapCar_Connection
{
    public class CustomerDAO
    {
        #region Customer

        public CustomerData getCustomer(int CUST_ID)
        {
            try
            {
                Entities context = new Entities();

                var result = (from x in context.TB_M_CUSTOMER
                              where x.CUS_ID == CUST_ID
                              select new CustomerData()
                              {
                                  CUS_FNAME = x.CUS_FNAME,
                                  CUS_ID = x.CUS_ID,
                                  CUS_LNAME = x.CUS_LNAME,
                                  DRIVING_LICENCE_EXP_DATE = x.DRIVING_LICENCE_EXP_DATE,
                                  DRIVING_LICENCE_ID = x.DRIVING_LICENCE_ID,
                                  DRIVING_LICENCE_ISSUE_DATE = x.DRIVING_LICENCE_ISSUE_DATE,
                                  EMAIL = x.EMAIL,
                                  ID_CARD = x.ID_CARD,
                                  ID_CARD_EXP_DATE = x.ID_CARD_EXP_DATE,
                                  ID_CARD_ISSUE_DATE = x.ID_CARD_ISSUE_DATE,
                                  MOBILE_NO = x.MOBILE_NO,
                                  REGISTER_DATE = x.REGISTER_DATE,
                                  TITLE = x.TITLE
                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CustomerData> getCustomerList()
        {
            try
            {
                Entities context = new Entities();

                var result = (from x in context.TB_M_CUSTOMER
                              select new CustomerData()
                              {
                                  CUS_FNAME = x.CUS_FNAME,
                                  CUS_ID = x.CUS_ID,
                                  CUS_LNAME = x.CUS_LNAME,
                                  DRIVING_LICENCE_EXP_DATE = x.DRIVING_LICENCE_EXP_DATE,
                                  DRIVING_LICENCE_ID = x.DRIVING_LICENCE_ID,
                                  DRIVING_LICENCE_ISSUE_DATE = x.DRIVING_LICENCE_ISSUE_DATE,
                                  EMAIL = x.EMAIL,
                                  ID_CARD = x.ID_CARD,
                                  ID_CARD_EXP_DATE = x.ID_CARD_EXP_DATE,
                                  ID_CARD_ISSUE_DATE = x.ID_CARD_ISSUE_DATE,
                                  MOBILE_NO = x.MOBILE_NO,
                                  REGISTER_DATE = x.REGISTER_DATE,
                                  TITLE = x.TITLE
                              }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CustomerCarData> getCustomerCar(int CUS_ID)
        {
            try
            {
                Entities context = new Entities();

                var result = (from x in context.TB_M_CUSTOMER_CAR
                              where x.CUS_ID == CUS_ID
                              select new CustomerCarData()
                              {
                                  CAR_BRAND = x.CAR_BRAND,
                                  CAR_COLOR = x.CAR_COLOR,
                                  CAR_COLOR_HEXCODE = x.CAR_COLOR_HEXCODE,
                                  CAR_LICENSE_NO = x.CAR_LICENSE_NO,
                                  CAR_MODEL = x.CAR_MODEL,
                                  CAR_YEAR = x.CAR_YEAR,
                                  CUS_CAR_ID = x.CUS_CAR_ID,
                                  CUS_ID = x.CUS_ID
                              }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public bool saveCustomer(TB_M_CUSTOMER data)
        {
            bool ret = false;
            try
            {
                Entities context = new Entities();
                context.TB_M_CUSTOMER.Add(data);
                context.SaveChanges();

                ret = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }
    }
}
